package com.sourceit;

import com.sourceit.model.Book;
import com.sourceit.model.Literature;
import com.sourceit.model.Magazine;

public class Main {

    public static void main(String[] args) {
        Literature[] literatures = new Literature[5];
        literatures[0] = new Book("Преступление и наказание", 1800, "Достоевский");
        literatures[1] = new Book("Двойник", 1850, "Достоевский");
        literatures[2] = new Book("Мёртвые души", 1890, "Гоголь");
        literatures[3] = new Magazine("Этот прекрасный мир", 2005, "Ранок" );
        literatures[4] = new Magazine("Мир животных", 2002, "Ранок" );

        String author = "Гоголь";
        //only books
        for (Literature literature : literatures) {
            if (literature instanceof Book) {
                Book book = (Book) literature;
                Literature lit = literature;
                Object object = literature;

                if (book.getAuthor().equalsIgnoreCase(author)) {
                    System.out.println(book);
                }
            }
//            if (literature.isOlderThen(1900)) {
//                System.out.println(literature);
//            }
        }
    }
}
