package com.sourceit.model;

public class Book extends Literature {

   private String author;

    public Book(String name, int yearOfPublishing, String author) {
        super(name, yearOfPublishing);
        this.author = author;
    }

    @Override
    public String toString() {
        return String.format("%s; author: %s", super.toString(), author);
    }

    public String getAuthor() {
        return author;
    }
}
