package com.sourceit.model;

public class Literature {
    private String name;
    private int yearOfPublishing;

    public Literature(String name, int yearOfPublishing) {
        this.name = name;
        this.yearOfPublishing = yearOfPublishing;
    }

    @Override
    public String toString() {
        return String.format("name: %s; year: %d", name, yearOfPublishing);
    }

    public String getName() {
        return name;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public boolean isOlderThen(int year) {
        return yearOfPublishing < year;
    }
}
