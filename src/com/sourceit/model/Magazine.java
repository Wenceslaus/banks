package com.sourceit.model;

public class Magazine extends Literature{
    private String publishingHouse;

    public Magazine(String name, int yearOfPublishing, String publishingHouse) {
        super(name, yearOfPublishing);
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return String.format("%s; publishingHouse: %s", super.toString(), publishingHouse);
    }
}
